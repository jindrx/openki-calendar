<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://jindrx.space
 * @since             1.0.0
 * @package           Openki_Calendar
 *
 * @wordpress-plugin
 * Plugin Name:       openki-calendar
 * Plugin URI:        https://https://openki.net/
 * Description:       This plugin creates a calendar out of an Openki dataset. ??
 * Version:           1.0.0
 * Author:            Jindra Dušek
 * Author URI:        https://jindrx.space/
 * License:           GPL-2.0+
 * License URI:       CC0
 * Text Domain:       openki-calendar
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'OPENKI_CALENDAR_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-openki-calendar-activator.php
 */
function activate_openki_calendar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-openki-calendar-activator.php';
	Openki_Calendar_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-openki-calendar-deactivator.php
 */
function deactivate_openki_calendar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-openki-calendar-deactivator.php';
	Openki_Calendar_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_openki_calendar' );
register_deactivation_hook( __FILE__, 'deactivate_openki_calendar' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-openki-calendar.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_openki_calendar() {

	$plugin = new Openki_Calendar();
	$plugin->run();

}
run_openki_calendar();
