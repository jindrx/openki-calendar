<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://jindrx.space
 * @since      1.0.0
 *
 * @package    Openki_Calendar
 * @subpackage Openki_Calendar/admin/partials
 */
?>

<head>
    <!-- our css -->
    <link rel="stylesheet" href=<?=plugins_url( '/css/openki.css', __FILE__ );?>>
    <!-- The momentjs library -->
    <script src=<?=plugins_url( '/js/moment-with-locales.min.js', __FILE__ );?>></script>

    <!-- Use a CDN instead -->
    <!--
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.30.1/moment-with-locales.min.js"></script>
    -->
</head>

<h1>Hello world</h1>


<div id="openkicontainer" class="openkicontainer"></div>

<div class="credits" id="credits">
    <div>Dataset by <a href="https://openki.net">Openki </a>
    <img class="by-sa-logo" src=<?=plugins_url( '/img/by-sa.png', __FILE__ );?>></div>
</div>


<script>
var locale = "de"; //Which locale are we using?
var groups = ["90aa41f3"]; //An Openki group ID, see https://gitlab.com/Openki/Openki/-/wikis/Calendar-export
var days = 5;  //How many days should be displayed?
var sort = ""; //Sorting function to be used in filter

//var moment_with_a_locale = moment.locale("de");

// Openki API url
var url = 'https://openki.net/api/0/json/events?';

function appendDiv(appendid, appendclass, appendcontainer) {
    var div = document.createElement('div');
    div.setAttribute("id", appendid);
    div.className = appendclass;
    var container = document.getElementById(appendcontainer);
    container.appendChild(div);
    return;
}

function addCalendarDay(weekday_id) {
    appendDiv(weekday_id, "day", "openkicontainer");
    appendDiv("morning"+weekday_id, "morning", weekday_id);
    appendDiv("afternoon"+weekday_id, "afternoon", weekday_id);
    appendDiv("evening"+weekday_id, "evening", weekday_id);
}

// add divs for requested days
for ( i = 1 ; i <= days ; i++ ) {
     addCalendarDay(i);
}

// XMLHttpRequest helper
var getJSON = function(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.responseType = 'json';

  xhr.onload = function() {
    var status = xhr.status;
    if (status == 200) {
      callback(null, xhr.response);
    } else {
      callback(status);
    }
  };
  xhr.send();
};

// Helper for check building blocks
function contains(arr, element) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === element) {
            return true;
        }
    }
    return false;
}

// Sort function, ASZ only

function getSortID(ptitle) {

    var sortid = 0;

    var pfeila = "Pfeil → A";
    var pfeilb = "Pfeil → B";
    var kreis = "Kreis";
    var dreieck = "Dreieck";
    var viereck = "Viereck";
    var spiral = "Spiral";

    if (ptitle.indexOf(pfeila) !== -1) {
        sortid = 1;
    } else if (ptitle.indexOf(pfeilb) !== -1) {
        sortid = 2;
    } else if (ptitle.indexOf(kreis) !== -1) {
        sortid = 3;
    } else if (ptitle.indexOf(dreieck) !== -1) {
        sortid = 4;
    } else if (ptitle.indexOf(viereck) !== -1) {
        sortid = 5;
    } else if (ptitle.indexOf(spiral) !== -1) {
        sortid = 6;
    } else {
        sortid = 999;
    }

    return sortid;
}

// helper for sorting by custom sort id

function sortByID(container) {
    var main = document.getElementById( container );

    [].map.call( main.children, Object ).sort( function ( a, b ) {
        return +a.id.match( /\d+/ ) - +b.id.match( /\d+/ );
    }).forEach( function ( elem ) {
        main.appendChild( elem );
    });
}

// Compose filters for the request
function filter(groups, after, before, sort) {
    var values = 'groups=' + groups + '&after=' + after + '&before=' + before + '&sort=' + sort;
    return values;
}

// Add content to '#day'-divs
function innerData(daytime, pos, content) {
    var html = document.getElementById( daytime+pos ).innerHTML += content;
};

// Getting the day for schedule
function parseWeekDay(url, weekday_id, pos) {
    var sortid = 0;
    day = moment().day(weekday_id).format('YYYY-MM-DD'); // get current week, first week day
    var url_full = url + filter(groups, day, moment(day).add(1, 'days').format('YYYY-MM-DD'), sort );
    getJSON(url_full,
    function(err, openkidata) {
    if (err != null) {
        console.log('Something went wrong with getJSON: ' + err);
    } else {
        var keys = Object.keys(openkidata.data);
        moment.locale(locale);
        var checktimes = [];
        var openkimorning = [];
        var openkiafternoon = [];
        var openkievening = [];
        var showweekday = true;
        var blocks = false;

        function getPartOfDay(startLocal, endLocal, title, filter, ddtime) {
            var content = [];

            function addCourseWithTime() {
                content += '<div id=\'' + sortid + '\' class=\'extduration-top\'>' + startLocal + ' - ' + endLocal + '</div>';
                sortid = sortid + 1;
                // Color filter
                if (title.startsWith(filter)) {
                    content += '<div id=\'' + sortid + '\' class=\'exttitle colorblock\'>' + title + '</div>';
                } else {
                    content += '<div id=\'' + sortid + '\' class=\'exttitle\'>' + title + '</div>';
                }

                // Push this start time to helper array for building blocks
                checktimes.push(startLocal+" - "+endLocal);
            }

            function addCourseWithoutTime() {
                sortid = sortid+1;
                // color filter
                if (title.startsWith(filter)) {
                    content += '<div id=\'' + sortid + '\' class=\'exttitle colorblock\'>' + title + '</div>';
                } else {
                    content += '<div id=\'' + sortid + '\' class=\'exttitle\'>' + title + '</div>';
                }
            }

            // filter by start time for building blocks
            if (contains(checktimes, startLocal+" - "+endLocal)) {
                addCourseWithoutTime();
            } else {
                addCourseWithTime();
                blocks = true;
            }
            return content;
        }

        function addBlockDaytime() {
            if (moment(openkidata.data[i].startLocal).hour() <= 11) {
                sortid = getSortID(ptitle);
                openkimorning += getPartOfDay(pstartLocal, pendLocal, ptitle, 'Deutsch', 'morning');
            }
            else if (moment(openkidata.data[i].startLocal).hour() > 11 && moment(openkidata.data[i].startLocal).hour() <= 16) {
                sortid = getSortID(ptitle);
                openkiafternoon += getPartOfDay(pstartLocal, pendLocal, ptitle, 'Deutsch', 'afternoon');
            }
            else if (moment(openkidata.data[i].startLocal).hour() > 16 && moment(openkidata.data[i].startLocal).hour() <= 21) {
                sortid = getSortID(ptitle);
                openkievening += getPartOfDay(pstartLocal, pendLocal, ptitle, 'Deutsch', 'evening');
            }
        }

        for ( i = 0 ; i < keys.length ; i++ ) {
            //var pdate = moment(openkidata.data[i].startLocal).format('dddd, L'); // with date
            var pdate = moment(openkidata.data[i].startLocal).format('dddd');
            var pstartLocal = moment(openkidata.data[i].startLocal).format('LT');
            var pendLocal = moment(openkidata.data[i].endLocal).format('LT');
            var ptitle = openkidata.data[i].title

            if (showweekday) {
                openkimorning += '<div class=\'extdate\'>' + pdate + '</div>';
                showweekday = false;
            }

            addBlockDaytime();

        };

        var content = { morning: openkimorning, afternoon: openkiafternoon, evening: openkievening };
        //console.log(content);

        innerData("morning", pos, content.morning);
        innerData("afternoon", pos, content.afternoon);
        innerData("evening", pos, content.evening);

        sortByID('morning1');
        sortByID('afternoon1');
        sortByID('morning3');
        sortByID('afternoon3');
        sortByID('morning5');
        sortByID('afternoon5');

        /*function addRecent() {
            var d = document.getElementById(moment().format('dddd'));
            d.className += " recent";
        }

        addRecent();*/


  }

});

};


// parse all requested days
for ( i = 1 ; i <= days; i++ ) {
     parseWeekDay(url, i, i);
}

</script>
