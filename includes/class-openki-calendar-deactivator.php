<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://jindrx.space
 * @since      1.0.0
 *
 * @package    Openki_Calendar
 * @subpackage Openki_Calendar/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Openki_Calendar
 * @subpackage Openki_Calendar/includes
 * @author     Jindra Dušek <jindra@immerda.ch>
 */
class Openki_Calendar_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
