<?php

/**
 * Fired during plugin activation
 *
 * @link       https://jindrx.space
 * @since      1.0.0
 *
 * @package    Openki_Calendar
 * @subpackage Openki_Calendar/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Openki_Calendar
 * @subpackage Openki_Calendar/includes
 * @author     Jindra Dušek <jindra@immerda.ch>
 */
class Openki_Calendar_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
